
class cadenaDiccionario:

    def __init__(self,N,K):
        self.N=N
        self.k = K
        self.chocolatinaRepetida = {} # Variable tipo diccionario que va a llenarse con la cantidad de caracteres repetidos de la cadena
    
# Definición de métodos o funciones a usar en la clase
    def llenar(self, cadena):
        for caracter in cadena:
            if caracter in self.chocolatinaRepetida:
                self.chocolatinaRepetida[caracter] += 1
            else:
                self.chocolatinaRepetida[caracter] = 1	
    
    def mostrar(self):
        print(self.chocolatinaRepetida)
        for campo,valor in self.chocolatinaRepetida.items():
            print (campo,"->",valor)

# Creacion de objetos o instancias de la clase
N, K = input().split()
N=int(N)
K=int(K)
w = cadenaDiccionario(N,K)
w.llenar()
w.mostrar()

# Borrado de objetos o instancias una vez usadas
del w